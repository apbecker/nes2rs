use crate::cart::Cartridge;
use crate::cpu::{Condition, Index, Instruction, Mode};

pub struct AsmOutput<'a> {
  pub cart: &'a Cartridge,
  pub subs: Vec<(u16, Vec<(u16, Instruction)>)>,
}

impl<'a> std::fmt::Display for AsmOutput<'a> {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    let nmi = self.cart.nmi_vector();
    let res = self.cart.res_vector();
    let irq = self.cart.irq_vector();

    writeln!(f, "; nmi = {}", AsmAbs(nmi))?;
    writeln!(f, "; res = {}", AsmAbs(res))?;
    writeln!(f, "; irq = {}", AsmAbs(irq))?;

    for e in self.subs.iter() {
      writeln!(f)?;
      writeln!(f, "_{:04x}:", e.0)?;

      for &(address, instruction) in e.1.iter() {
        writeln!(f, "  ; {}: {}", AsmAbs(address), AsmCode(instruction))?;
      }
    }

    Ok(())
  }
}

struct AsmCode(Instruction);

impl std::fmt::Display for AsmCode {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self.0 {
      Instruction::ADC(mode) => write!(f, "adc {}", AsmMode(mode)),
      Instruction::AND(mode) => write!(f, "and {}", AsmMode(mode)),
      Instruction::ASL(mode) => write!(f, "asl {}", AsmMode(mode)),
      Instruction::ASLA => write!(f, "asl a"),
      Instruction::BIT(mode) => write!(f, "bit {}", AsmMode(mode)),
      Instruction::BRK(mark) => write!(f, "brk #${:02x}", mark),
      Instruction::BXX(target, Condition::CC) => write!(f, "bcc {}", AsmAbs(target)),
      Instruction::BXX(target, Condition::CS) => write!(f, "bcs {}", AsmAbs(target)),
      Instruction::BXX(target, Condition::EQ) => write!(f, "beq {}", AsmAbs(target)),
      Instruction::BXX(target, Condition::MI) => write!(f, "bmi {}", AsmAbs(target)),
      Instruction::BXX(target, Condition::NE) => write!(f, "bne {}", AsmAbs(target)),
      Instruction::BXX(target, Condition::PL) => write!(f, "bpl {}", AsmAbs(target)),
      Instruction::BXX(target, Condition::VC) => write!(f, "bvc {}", AsmAbs(target)),
      Instruction::BXX(target, Condition::VS) => write!(f, "bvs {}", AsmAbs(target)),
      Instruction::CLC => write!(f, "clc"),
      Instruction::CLD => write!(f, "cld"),
      Instruction::CLI => write!(f, "cli"),
      Instruction::CLV => write!(f, "clv"),
      Instruction::CMP(mode) => write!(f, "cmp {}", AsmMode(mode)),
      Instruction::CPX(mode) => write!(f, "cpx {}", AsmMode(mode)),
      Instruction::CPY(mode) => write!(f, "cpy {}", AsmMode(mode)),
      Instruction::DEC(mode) => write!(f, "dec {}", AsmMode(mode)),
      Instruction::DEX => write!(f, "dex"),
      Instruction::DEY => write!(f, "dey"),
      Instruction::EOR(mode) => write!(f, "eor {}", AsmMode(mode)),
      Instruction::INC(mode) => write!(f, "inc {}", AsmMode(mode)),
      Instruction::INX => write!(f, "inx"),
      Instruction::INY => write!(f, "iny"),
      Instruction::JMP(target) => write!(f, "jmp {}", AsmAbs(target)),
      Instruction::JSR(target) => write!(f, "jsr {}", AsmAbs(target)),
      Instruction::LDA(mode) => write!(f, "lda {}", AsmMode(mode)),
      Instruction::LDX(mode) => write!(f, "ldx {}", AsmMode(mode)),
      Instruction::LDY(mode) => write!(f, "ldy {}", AsmMode(mode)),
      Instruction::LSR(mode) => write!(f, "lsr {}", AsmMode(mode)),
      Instruction::LSRA => write!(f, "lsr a"),
      Instruction::NOP => write!(f, "nop"),
      Instruction::ORA(mode) => write!(f, "ora {}", AsmMode(mode)),
      Instruction::PHA => write!(f, "pha"),
      Instruction::PHP => write!(f, "php"),
      Instruction::PLA => write!(f, "pla"),
      Instruction::PLP => write!(f, "plp"),
      Instruction::ROL(mode) => write!(f, "rol {}", AsmMode(mode)),
      Instruction::ROLA => write!(f, "rol a"),
      Instruction::ROR(mode) => write!(f, "ror {}", AsmMode(mode)),
      Instruction::RORA => write!(f, "ror a"),
      Instruction::RTI => write!(f, "rti"),
      Instruction::RTS => write!(f, "rts"),
      Instruction::SBC(mode) => write!(f, "sbc {}", AsmMode(mode)),
      Instruction::SEC => write!(f, "sec"),
      Instruction::SED => write!(f, "sed"),
      Instruction::SEI => write!(f, "sei"),
      Instruction::STA(mode) => write!(f, "sta {}", AsmMode(mode)),
      Instruction::STX(mode) => write!(f, "stx {}", AsmMode(mode)),
      Instruction::STY(mode) => write!(f, "sty {}", AsmMode(mode)),
      Instruction::TAX => write!(f, "tax"),
      Instruction::TAY => write!(f, "tay"),
      Instruction::TSX => write!(f, "tsx"),
      Instruction::TXA => write!(f, "txa"),
      Instruction::TXS => write!(f, "txs"),
      Instruction::TYA => write!(f, "tya"),
      Instruction::VIA(target) => write!(f, "jmp ({})", AsmAbs(target)),
      Instruction::Junk(_, _) => Ok(()),
    }
  }
}

struct AsmMode(Mode);

impl std::fmt::Display for AsmMode {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self.0 {
      Mode::Absolute(address) => write!(f, "{}", AsmAbs(address)),
      Mode::AbsoluteIndexed(address, Index::X) => write!(f, "{},x", AsmAbs(address)),
      Mode::AbsoluteIndexed(address, Index::Y) => write!(f, "{},y", AsmAbs(address)),
      Mode::Constant(data) => write!(f, "#${:02x}", data),
      Mode::IndirectIndexed(address, Index::X) => write!(f, "({},x)", AsmZpg(address)),
      Mode::IndirectIndexed(address, Index::Y) => write!(f, "({}),y", AsmZpg(address)),
      Mode::ZeroPage(address) => write!(f, "{}", AsmZpg(address)),
      Mode::ZeroPageIndexed(address, Index::X) => write!(f, "{},x", AsmZpg(address)),
      Mode::ZeroPageIndexed(address, Index::Y) => write!(f, "{},y", AsmZpg(address)),
    }
  }
}

struct AsmAbs(u16);

impl std::fmt::Display for AsmAbs {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "${:04x}", self.0)
  }
}

struct AsmZpg(u8);

impl std::fmt::Display for AsmZpg {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "${:02x}", self.0)
  }
}
