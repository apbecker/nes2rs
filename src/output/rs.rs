use std::collections::HashSet;

use crate::cart::Cartridge;
use crate::cpu::{Condition, Index, Instruction, Mode};

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct RustRef {
  pub src: u16,
  pub dst: u16,
}

pub struct RustOutput<'a> {
  pub cart: &'a Cartridge,
  pub subs: Vec<(u16, Vec<(u16, Instruction)>)>,
  pub refs: HashSet<RustRef>,
}

impl<'a> std::fmt::Display for RustOutput<'a> {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    let nmi = RustVector {
      name: "nmi",
      address: self.cart.nmi_vector(),
    };

    let res = RustVector {
      name: "res",
      address: self.cart.res_vector(),
    };

    let irq = RustVector {
      name: "irq",
      address: self.cart.irq_vector(),
    };

    writeln!(f, "use crate::rt;")?;
    writeln!(f)?;
    writeln!(f, "#[derive(Default)]")?;
    writeln!(f, "pub struct Game;")?;
    writeln!(f)?;
    writeln!(f, "impl Game {{")?;
    writeln!(f, "{nmi}")?;
    writeln!(f, "{res}")?;
    writeln!(f, "{irq}")?;

    for (address, code) in self.subs.iter() {
      writeln!(f)?;
      writeln!(f, "  fn _{:04x}(&self, o: &mut rt::Op, a: &mut rt::Am) {{", address)?;

      for &(address, instruction) in code.iter() {
        self.append_comment(f, address)?;
        writeln!(f, "    {};", RustCode::Instruction(instruction))?;
      }

      writeln!(f, "  }}")?;
    }

    writeln!(f, "}}")?;

    Ok(())
  }
}

impl<'a> RustOutput<'a> {
  fn append_comment(&self, f: &mut std::fmt::Formatter, address: u16) -> std::fmt::Result {
    let src = self.get_src_ref(address);
    let dst = self.get_dst_refs(address);

    if src.is_none() && dst.is_empty() {
      return Ok(());
    }

    write!(f, "    // {}.", RustAbs(address))?;
    self.append_refers_to(f, src)?;
    self.append_referred_to_by(f, dst)?;
    writeln!(f)?;
    Ok(())
  }

  fn append_refers_to(&self, f: &mut std::fmt::Formatter, src: Option<RustRef>) -> std::fmt::Result {
    if let Some(e) = src {
      write!(f, " refers to {}.", RustAbs(e.dst))?;
    }

    Ok(())
  }

  fn get_src_ref(&self, address: u16) -> Option<RustRef> {
    self.refs.iter().find(|e| e.src == address).map(|e| e.clone())
  }

  fn append_referred_to_by(&self, f: &mut std::fmt::Formatter, dsts: Vec<RustAbs>) -> std::fmt::Result {
    let mut as_dst = dsts.into_iter();
    let Some(e) = as_dst.next() else {
      return Ok(());
    };

    write!(f, " referred to by {}", e)?;

    while let Some(e) = as_dst.next() {
      write!(f, ", {}", e)?;
    }

    write!(f, ".")?;

    Ok(())
  }

  fn get_dst_refs(&self, address: u16) -> Vec<RustAbs> {
    let mut as_dst = self
      .refs
      .iter()
      .filter(|e| e.dst == address)
      .map(|e| RustAbs(e.src))
      .collect::<Vec<_>>();

    as_dst.sort_by_key(|e| e.0);
    as_dst
  }
}

pub struct RustVector {
  name: &'static str,
  address: u16,
}

impl std::fmt::Display for RustVector {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    let name = self.name;
    let address = self.address;

    writeln!(f, "  pub fn {}(&mut self, o: &mut rt::Op, a: &mut rt::Am) {{", name)?;
    writeln!(f, "    self._{:04x}(o, a)", address)?;
    writeln!(f, "  }}")
  }
}

pub enum RustCode {
  EarlyReturn(Condition),
  Instruction(Instruction),
}

impl std::fmt::Display for RustCode {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self {
      Self::EarlyReturn(condition) => match condition {
        Condition::CC => write!(f, "if o.cc() {{ o.rts() }}"),
        Condition::CS => write!(f, "if o.cs() {{ o.rts() }}"),
        Condition::EQ => write!(f, "if o.eq() {{ o.rts() }}"),
        Condition::MI => write!(f, "if o.mi() {{ o.rts() }}"),
        Condition::NE => write!(f, "if o.ne() {{ o.rts() }}"),
        Condition::PL => write!(f, "if o.pl() {{ o.rts() }}"),
        Condition::VC => write!(f, "if o.vc() {{ o.rts() }}"),
        Condition::VS => write!(f, "if o.vs() {{ o.rts() }}"),
      },
      Self::Instruction(instruction) => match instruction.clone() {
        Instruction::ADC(mode) => write!(f, "o.adc({})", RustMode(mode)),
        Instruction::AND(mode) => write!(f, "o.and({})", RustMode(mode)),
        Instruction::ASL(mode) => write!(f, "o.asl({})", RustMode(mode)),
        Instruction::ASLA => write!(f, "o.asl_a()"),
        Instruction::BIT(mode) => write!(f, "o.bit({})", RustMode(mode)),
        Instruction::BRK(mark) => write!(f, "o.brk(0x{:02x})", mark),
        Instruction::BXX(target, condition) => match condition {
          Condition::CC => write!(f, "if o.cc() {{ o.jmp({}) }}", RustAbs(target)),
          Condition::CS => write!(f, "if o.cs() {{ o.jmp({}) }}", RustAbs(target)),
          Condition::EQ => write!(f, "if o.eq() {{ o.jmp({}) }}", RustAbs(target)),
          Condition::MI => write!(f, "if o.mi() {{ o.jmp({}) }}", RustAbs(target)),
          Condition::NE => write!(f, "if o.ne() {{ o.jmp({}) }}", RustAbs(target)),
          Condition::PL => write!(f, "if o.pl() {{ o.jmp({}) }}", RustAbs(target)),
          Condition::VC => write!(f, "if o.vc() {{ o.jmp({}) }}", RustAbs(target)),
          Condition::VS => write!(f, "if o.vs() {{ o.jmp({}) }}", RustAbs(target)),
        },
        Instruction::CLC => write!(f, "o.clc()"),
        Instruction::CLD => write!(f, "o.cld()"),
        Instruction::CLI => write!(f, "o.cli()"),
        Instruction::CLV => write!(f, "o.clv()"),
        Instruction::CMP(mode) => write!(f, "o.cmp({})", RustMode(mode)),
        Instruction::CPX(mode) => write!(f, "o.cpx({})", RustMode(mode)),
        Instruction::CPY(mode) => write!(f, "o.cpy({})", RustMode(mode)),
        Instruction::DEC(mode) => write!(f, "o.dec({})", RustMode(mode)),
        Instruction::DEX => write!(f, "o.dex()"),
        Instruction::DEY => write!(f, "o.dey()"),
        Instruction::EOR(mode) => write!(f, "o.eor({})", RustMode(mode)),
        Instruction::INC(mode) => write!(f, "o.inc({})", RustMode(mode)),
        Instruction::INX => write!(f, "o.inx()"),
        Instruction::INY => write!(f, "o.iny()"),
        Instruction::JMP(target) => write!(f, "o.jmp({})", RustAbs(target)),
        Instruction::JSR(target) => write!(f, "o.jsr({})", RustAbs(target)),
        Instruction::LDA(mode) => write!(f, "o.lda({})", RustMode(mode)),
        Instruction::LDX(mode) => write!(f, "o.ldx({})", RustMode(mode)),
        Instruction::LDY(mode) => write!(f, "o.ldy({})", RustMode(mode)),
        Instruction::LSR(mode) => write!(f, "o.lsr({})", RustMode(mode)),
        Instruction::LSRA => write!(f, "o.lsr_a()"),
        Instruction::NOP => write!(f, "o.nop()"),
        Instruction::ORA(mode) => write!(f, "o.ora({})", RustMode(mode)),
        Instruction::PHA => write!(f, "o.pha()"),
        Instruction::PHP => write!(f, "o.php()"),
        Instruction::PLA => write!(f, "o.pla()"),
        Instruction::PLP => write!(f, "o.plp()"),
        Instruction::ROL(mode) => write!(f, "o.rol({})", RustMode(mode)),
        Instruction::ROLA => write!(f, "o.rol_a()"),
        Instruction::ROR(mode) => write!(f, "o.ror({})", RustMode(mode)),
        Instruction::RORA => write!(f, "o.ror_a()"),
        Instruction::RTI => write!(f, "o.rti()"),
        Instruction::RTS => write!(f, "o.rts()"),
        Instruction::SBC(mode) => write!(f, "o.sbc({})", RustMode(mode)),
        Instruction::SEC => write!(f, "o.sec()"),
        Instruction::SED => write!(f, "o.sed()"),
        Instruction::SEI => write!(f, "o.sei()"),
        Instruction::STA(mode) => write!(f, "o.sta({})", RustMode(mode)),
        Instruction::STX(mode) => write!(f, "o.stx({})", RustMode(mode)),
        Instruction::STY(mode) => write!(f, "o.sty({})", RustMode(mode)),
        Instruction::TAX => write!(f, "o.tax()"),
        Instruction::TAY => write!(f, "o.tay()"),
        Instruction::TSX => write!(f, "o.tsx()"),
        Instruction::TXA => write!(f, "o.txa()"),
        Instruction::TXS => write!(f, "o.txs()"),
        Instruction::TYA => write!(f, "o.tya()"),
        Instruction::VIA(_) => todo!(),
        Instruction::Junk(_, _) => Ok(()),
      },
    }
  }
}

pub struct RustMode(Mode);

impl std::fmt::Display for RustMode {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self.0 {
      Mode::Absolute(address) => write!(f, "a.abs({})", RustAbs(address)),
      Mode::AbsoluteIndexed(address, Index::X) => write!(f, "a.abx({})", RustAbs(address)),
      Mode::AbsoluteIndexed(address, Index::Y) => write!(f, "a.aby({})", RustAbs(address)),
      Mode::Constant(n) => write!(f, "{}", RustConstant(n)),
      Mode::IndirectIndexed(address, Index::X) => write!(f, "a.inx({})", RustZpg(address)),
      Mode::IndirectIndexed(address, Index::Y) => write!(f, "a.iny({})", RustZpg(address)),
      Mode::ZeroPage(address) => write!(f, "a.zpg({})", RustZpg(address)),
      Mode::ZeroPageIndexed(address, Index::X) => write!(f, "a.zpx({})", RustZpg(address)),
      Mode::ZeroPageIndexed(address, Index::Y) => write!(f, "a.zpy({})", RustZpg(address)),
    }
  }
}

pub struct RustConstant(u8);

impl std::fmt::Display for RustConstant {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    if self.0 < 16 {
      write!(f, "{}", self.0)
    } else {
      write!(f, "0x{:x}", self.0)
    }
  }
}

pub struct RustAbs(u16);

impl std::fmt::Display for RustAbs {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    if self.0 < 16 {
      write!(f, "{}", self.0)
    } else {
      write!(f, "0x{:x}", self.0)
    }
  }
}

pub struct RustZpg(u8);

impl std::fmt::Display for RustZpg {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    if self.0 < 16 {
      write!(f, "{}", self.0)
    } else {
      write!(f, "0x{:x}", self.0)
    }
  }
}
