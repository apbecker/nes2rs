use std::{fs, io, path};

use crate::{cart::Cartridge, util::io::ReadExactVec};

pub enum Error {
  InvalidFile,
  Open(io::Error),
  Read(io::Error),
  UnsupportedMapper(u8),
  UnsupportedMapperLayout,
}

pub struct NesFileHeader(Vec<u8>);

impl NesFileHeader {
  pub fn try_read_from(r: &mut impl io::Read) -> Result<NesFileHeader, Error> {
    let buffer = r.read_exact_vec(16).map_err(Error::Read)?;
    let header = NesFileHeader(buffer);
    Ok(header)
  }

  pub fn magic_bytes(&self) -> &[u8] {
    &self.0[0..4]
  }

  pub fn prg_len(&self) -> usize {
    self.0[4] as usize * 16384
  }

  pub fn chr_len(&self) -> usize {
    self.0[5] as usize * 8192
  }

  pub fn mapper(&self) -> u8 {
    let lower = self.0[6] >> 4;
    let upper = self.0[7] >> 4;
    (upper << 4) | lower
  }
}

pub struct NesFile {
  header: NesFileHeader,
  prg: Vec<u8>,
  chr: Vec<u8>,
}

impl NesFile {
  pub fn try_from_file(p: impl AsRef<path::Path>) -> Result<Self, Error> {
    let file = fs::File::open(p).map_err(Error::Open)?;

    Self::try_read_from(file)
  }

  pub fn try_read_from(mut r: impl io::Read) -> Result<Self, Error> {
    let header = NesFileHeader::try_read_from(&mut r)?;

    assert_eq!(b"NES\x1a", header.magic_bytes());

    let prg = r.read_exact_vec(header.prg_len()).map_err(Error::Read)?;
    let chr = r.read_exact_vec(header.chr_len()).map_err(Error::Read)?;

    Ok(Self { header, prg, chr })
  }
}

impl TryInto<Cartridge> for NesFile {
  type Error = Error;

  fn try_into(self) -> Result<Cartridge, Self::Error> {
    if self.header.mapper() != 0 {
      return Err(Error::UnsupportedMapper(self.header.mapper()));
    }

    if self.header.prg_len() != 32768 {
      return Err(Error::UnsupportedMapperLayout);
    }

    Ok(Cartridge::NROM256 {
      chr: self.chr,
      prg: self.prg,
    })
  }
}
