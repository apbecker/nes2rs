/// Represents the possible cartridge layouts.
pub enum Cartridge {
  /// No mapping hardware.
  NROM256 { chr: Vec<u8>, prg: Vec<u8> },
}

impl Cartridge {
  pub fn nmi_vector(&self) -> u16 {
    self.prg_word(0xfffa)
  }

  pub fn res_vector(&self) -> u16 {
    self.prg_word(0xfffc)
  }

  pub fn irq_vector(&self) -> u16 {
    self.prg_word(0xfffe)
  }

  pub fn prg_word(&self, address: u16) -> u16 {
    let lower = self.prg_byte(address);
    let upper = self.prg_byte(address.wrapping_add(1));
    u16::from_le_bytes([lower, upper])
  }

  pub fn prg_byte(&self, address: u16) -> u8 {
    match self {
      Self::NROM256 { prg, .. } => prg[(address as usize) & 0x7fff],
    }
  }
}
