use nes2rs::{cfg::ControlFlowGraph, ines::NesFile};

fn main() {
  let Some(path) = std::env::args().nth(1) else {
    println!("path not specified");
    return;
  };

  let Ok(file) = NesFile::try_from_file(path) else {
    println!("file is inaccessible, or invalid");
    return;
  };

  let Ok(cart) = file.try_into() else {
    println!("file isn't supported");
    return;
  };

  let cfg = ControlFlowGraph::from_cartridge(&cart);
  println!("{:?}", cfg)
}
