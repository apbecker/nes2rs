#![allow(dead_code)]

use crate::{
  cart::Cartridge,
  cpu::{Condition, Index, Mode},
};

#[derive(Clone, Default, PartialEq)]
pub struct State {
  // registers
  a: Value<u8>,
  s: Value<u8>,
  x: Value<u8>,
  y: Value<u8>,
  // flags
  n: Value<bool>,
  v: Value<bool>,
  d: Value<bool>,
  i: Value<bool>,
  z: Value<bool>,
  c: Value<bool>,
}

impl State {
  fn mode(&self, cart: &Cartridge, mode: Mode) -> Value<u8> {
    match mode {
      Mode::Absolute(address) => {
        if address >= 0x8000 {
          Value::Known(cart.prg_byte(address))
        } else {
          Value::Unknown
        }
      }
      Mode::AbsoluteIndexed(target, index) => match self.index(index) {
        Value::Unknown => Value::Unknown,
        Value::Known(index) => {
          let address = target.wrapping_add(index as u16);
          if address >= 0x8000 {
            Value::Known(cart.prg_byte(address))
          } else {
            Value::Unknown
          }
        }
      },
      Mode::Constant(val) => Value::Known(val),
      Mode::IndirectIndexed(_, _) => Value::Unknown,
      Mode::ZeroPage(_) => Value::Unknown,
      Mode::ZeroPageIndexed(_, _) => Value::Unknown,
    }
  }

  fn condition(&self, condition: Condition) -> Value<bool> {
    match condition {
      Condition::PL => self.n.map(|n| !n),
      Condition::MI => self.n,
      Condition::VC => self.v.map(|v| !v),
      Condition::VS => self.v,
      Condition::NE => self.z.map(|z| !z),
      Condition::EQ => self.z,
      Condition::CC => self.c.map(|c| !c),
      Condition::CS => self.c,
    }
  }

  fn index(&self, index: Index) -> Value<u8> {
    match index {
      Index::X => self.x,
      Index::Y => self.y,
    }
  }

  fn combine_lossy(&self, state: &Self) -> Self {
    Self {
      a: self.a.combine_lossy(state.a),
      s: self.s.combine_lossy(state.s),
      x: self.x.combine_lossy(state.x),
      y: self.y.combine_lossy(state.y),
      n: self.n.combine_lossy(state.n),
      v: self.v.combine_lossy(state.v),
      d: self.d.combine_lossy(state.d),
      i: self.i.combine_lossy(state.i),
      z: self.z.combine_lossy(state.z),
      c: self.c.combine_lossy(state.c),
    }
  }

  fn adc(&mut self, _: Value<u8>) {
    self.v = Value::Unknown;
    self.c = Value::Unknown;

    self.lda(Value::Unknown)
  }

  fn and(&mut self, m: Value<u8>) {
    let val = self.a.map2(m, |a, m| a & m);
    self.lda(val)
  }

  fn asl(&mut self, _: Mode) {
    self.n = Value::Unknown;
    self.z = Value::Unknown;
    self.c = Value::Unknown;
  }

  fn asl_a(&mut self) {
    self.c = Value::Unknown;
    self.lda(Value::Unknown)
  }

  fn bit(&mut self, m: Value<u8>) {
    self.n = m.map(|m| (m & 0x80) != 0);
    self.v = m.map(|m| (m & 0x40) != 0);
    self.z = self.a.map2(m, |a, m| (a & m) == 0);
  }

  fn clc(&mut self) {
    self.c = Value::Known(false)
  }

  fn cld(&mut self) {
    self.d = Value::Known(false)
  }

  fn cli(&mut self) {
    self.i = Value::Known(false)
  }

  fn clv(&mut self) {
    self.v = Value::Known(false)
  }

  fn cmp(&mut self, m: Value<u8>) {
    self.n = self.a.map2(m, |a, m| a < m);
    self.z = self.a.map2(m, |a, m| a == m);
    self.c = self.a.map2(m, |a, m| a >= m);
  }

  fn cpx(&mut self, m: Value<u8>) {
    self.n = self.x.map2(m, |x, m| x < m);
    self.z = self.x.map2(m, |x, m| x == m);
    self.c = self.x.map2(m, |x, m| x >= m);
  }

  fn cpy(&mut self, m: Value<u8>) {
    self.n = self.y.map2(m, |y, m| y < m);
    self.z = self.y.map2(m, |y, m| y == m);
    self.c = self.y.map2(m, |y, m| y >= m);
  }

  fn dec(&mut self, _: Mode) {
    self.n = Value::Unknown;
    self.z = Value::Unknown;
  }

  fn dex(&mut self) {
    let val = self.x.map(|x| x.wrapping_sub(1));
    self.ldx(val)
  }

  fn dey(&mut self) {
    let val = self.y.map(|y| y.wrapping_sub(1));
    self.ldy(val)
  }

  fn eor(&mut self, m: Value<u8>) {
    let val = self.a.map2(m, |a, m| a ^ m);
    self.lda(val)
  }

  fn inc(&mut self, _: Mode) {
    self.n = Value::Unknown;
    self.z = Value::Unknown;
  }

  fn inx(&mut self) {
    let val = self.x.map(|x| x.wrapping_add(1));
    self.ldx(val)
  }

  fn iny(&mut self) {
    let val = self.y.map(|y| y.wrapping_add(1));
    self.ldy(val)
  }

  fn lda(&mut self, val: Value<u8>) {
    self.a = val;
    self.n = self.a.n();
    self.z = self.a.z();
  }

  fn ldx(&mut self, val: Value<u8>) {
    self.x = val;
    self.n = self.x.n();
    self.z = self.x.z();
  }

  fn ldy(&mut self, val: Value<u8>) {
    self.y = val;
    self.n = self.y.n();
    self.z = self.y.z();
  }

  fn lsr(&mut self, _: Mode) {
    self.n = Value::Unknown;
    self.z = Value::Unknown;
    self.c = Value::Unknown;
  }

  fn lsr_a(&mut self) {
    self.c = Value::Unknown;
    self.lda(Value::Unknown)
  }

  fn nop(&mut self) {}

  fn ora(&mut self, m: Value<u8>) {
    let val = self.a.map2(m, |a, m| a | m);
    self.lda(val);
  }

  fn pha(&mut self) {}

  fn php(&mut self) {}

  fn pla(&mut self) {
    self.lda(Value::Unknown)
  }

  fn plp(&mut self) {
    self.n = Value::Unknown;
    self.v = Value::Unknown;
    self.d = Value::Unknown;
    self.i = Value::Unknown;
    self.z = Value::Unknown;
    self.c = Value::Unknown;
  }

  fn rol(&mut self, _: Mode) {
    self.n = Value::Unknown;
    self.z = Value::Unknown;
    self.c = Value::Unknown;
  }

  fn rol_a(&mut self) {
    self.c = Value::Unknown;
    self.lda(Value::Unknown);
  }

  fn ror(&mut self, _: Mode) {
    self.n = Value::Unknown;
    self.z = Value::Unknown;
    self.c = Value::Unknown;
  }

  fn ror_a(&mut self) {
    self.c = Value::Unknown;
    self.lda(Value::Unknown);
  }

  fn sbc(&mut self, _: Value<u8>) {
    self.v = Value::Unknown;
    self.c = Value::Unknown;
    self.lda(Value::Unknown);
  }

  fn sec(&mut self) {
    self.c = Value::Known(true)
  }

  fn sed(&mut self) {
    self.d = Value::Known(true)
  }

  fn sei(&mut self) {
    self.i = Value::Known(true)
  }

  fn sta(&mut self, _: Mode) {}

  fn stx(&mut self, _: Mode) {}

  fn sty(&mut self, _: Mode) {}

  fn tax(&mut self) {
    self.ldx(self.a)
  }

  fn tay(&mut self) {
    self.ldy(self.a)
  }

  fn tsx(&mut self) {
    self.ldx(self.s)
  }

  fn txa(&mut self) {
    self.lda(self.x)
  }

  fn txs(&mut self) {
    self.s = self.x;
  }

  fn tya(&mut self) {
    self.lda(self.y)
  }
}

#[derive(Clone, Copy, Default, PartialEq)]
pub enum Value<T> {
  #[default]
  Unknown,
  Known(T),
}

impl<T> Value<T> {
  #[inline]
  fn map<Result>(self, f: impl FnOnce(T) -> Result) -> Value<Result> {
    match self {
      Value::Unknown => Value::Unknown,
      Value::Known(x) => Value::Known(f(x)),
    }
  }

  #[inline]
  fn map2<U, Result>(self, other: Value<U>, f: impl FnOnce(T, U) -> Result) -> Value<Result> {
    match (self, other) {
      (Value::Known(a), Value::Known(b)) => Value::Known(f(a, b)),
      _ => Value::Unknown,
    }
  }
}

impl<T: PartialEq> Value<T> {
  fn combine_lossy(self, other: Self) -> Self {
    let Self::Known(a) = self else {
      return Self::Unknown;
    };

    let Self::Known(b) = other else {
      return Self::Unknown;
    };

    if a == b {
      Self::Known(a)
    } else {
      Self::Unknown
    }
  }
}

impl Value<u8> {
  fn n(self) -> Value<bool> {
    self.map(|e| e >= 0x80)
  }

  fn z(self) -> Value<bool> {
    self.map(|e| e == 0x00)
  }
}
