use crate::cart::Cartridge;

pub trait Addressable {
  fn prg_byte(&self, address: u16) -> u8;
}

pub struct ProgramCursor<'a> {
  cart: &'a Cartridge,
  address: u16,
}

impl<'a> ProgramCursor<'a> {
  pub fn new(cart: &'a Cartridge, address: u16) -> Self {
    Self { cart, address }
  }

  pub fn address(&self) -> u16 {
    self.address
  }

  pub fn next_instruction(&mut self) -> Instruction {
    let val = self.next_byte();

    match TABLE[val as usize] {
      // control flow
      (OP::BRK, AM::IMM) => Instruction::BRK(self.next_byte()),
      (OP::BCC, AM::REL) => Instruction::BXX(self.next_branch_target(), Condition::CC),
      (OP::BCS, AM::REL) => Instruction::BXX(self.next_branch_target(), Condition::CS),
      (OP::BEQ, AM::REL) => Instruction::BXX(self.next_branch_target(), Condition::EQ),
      (OP::BNE, AM::REL) => Instruction::BXX(self.next_branch_target(), Condition::NE),
      (OP::BVC, AM::REL) => Instruction::BXX(self.next_branch_target(), Condition::VC),
      (OP::BVS, AM::REL) => Instruction::BXX(self.next_branch_target(), Condition::VS),
      (OP::BMI, AM::REL) => Instruction::BXX(self.next_branch_target(), Condition::MI),
      (OP::BPL, AM::REL) => Instruction::BXX(self.next_branch_target(), Condition::PL),
      (OP::JMP, AM::ABS) => Instruction::JMP(self.next_word()),
      (OP::JMP, AM::IND) => Instruction::VIA(self.next_word()),
      (OP::JSR, AM::ABS) => Instruction::JSR(self.next_word()),
      (OP::RTI, AM::IMP) => Instruction::RTI,
      (OP::RTS, AM::IMP) => Instruction::RTS,
      // implied
      (OP::CLC, AM::IMP) => Instruction::CLC,
      (OP::CLD, AM::IMP) => Instruction::CLD,
      (OP::CLI, AM::IMP) => Instruction::CLI,
      (OP::CLV, AM::IMP) => Instruction::CLV,
      (OP::DEX, AM::IMP) => Instruction::DEX,
      (OP::DEY, AM::IMP) => Instruction::DEY,
      (OP::INX, AM::IMP) => Instruction::INX,
      (OP::INY, AM::IMP) => Instruction::INY,
      (OP::NOP, AM::IMP) => Instruction::NOP,
      (OP::PHA, AM::IMP) => Instruction::PHA,
      (OP::PHP, AM::IMP) => Instruction::PHP,
      (OP::PLA, AM::IMP) => Instruction::PLA,
      (OP::PLP, AM::IMP) => Instruction::PLP,
      (OP::SEC, AM::IMP) => Instruction::SEC,
      (OP::SED, AM::IMP) => Instruction::SED,
      (OP::SEI, AM::IMP) => Instruction::SEI,
      (OP::TAX, AM::IMP) => Instruction::TAX,
      (OP::TAY, AM::IMP) => Instruction::TAY,
      (OP::TSX, AM::IMP) => Instruction::TSX,
      (OP::TXA, AM::IMP) => Instruction::TXA,
      (OP::TXS, AM::IMP) => Instruction::TXS,
      (OP::TYA, AM::IMP) => Instruction::TYA,
      // accumulator
      (OP::ASL, AM::IMP) => Instruction::ASLA,
      (OP::LSR, AM::IMP) => Instruction::LSRA,
      (OP::ROL, AM::IMP) => Instruction::ROLA,
      (OP::ROR, AM::IMP) => Instruction::RORA,
      // alu
      (OP::ADC, mode) => Instruction::ADC(self.address_mode(mode)),
      (OP::AND, mode) => Instruction::AND(self.address_mode(mode)),
      (OP::ASL, mode) => Instruction::ASL(self.address_mode(mode)),
      (OP::BIT, mode) => Instruction::BIT(self.address_mode(mode)),
      (OP::CMP, mode) => Instruction::CMP(self.address_mode(mode)),
      (OP::CPX, mode) => Instruction::CPX(self.address_mode(mode)),
      (OP::CPY, mode) => Instruction::CPY(self.address_mode(mode)),
      (OP::DEC, mode) => Instruction::DEC(self.address_mode(mode)),
      (OP::EOR, mode) => Instruction::EOR(self.address_mode(mode)),
      (OP::INC, mode) => Instruction::INC(self.address_mode(mode)),
      (OP::LSR, mode) => Instruction::LSR(self.address_mode(mode)),
      (OP::ORA, mode) => Instruction::ORA(self.address_mode(mode)),
      (OP::ROL, mode) => Instruction::ROL(self.address_mode(mode)),
      (OP::ROR, mode) => Instruction::ROR(self.address_mode(mode)),
      (OP::SBC, mode) => Instruction::SBC(self.address_mode(mode)),
      // memory
      (OP::LDA, mode) => Instruction::LDA(self.address_mode(mode)),
      (OP::LDX, mode) => Instruction::LDX(self.address_mode(mode)),
      (OP::LDY, mode) => Instruction::LDY(self.address_mode(mode)),
      (OP::STA, mode) => Instruction::STA(self.address_mode(mode)),
      (OP::STX, mode) => Instruction::STX(self.address_mode(mode)),
      (OP::STY, mode) => Instruction::STY(self.address_mode(mode)),
      // unintended instructions, will not be implemented.
      (code, mode) => {
        self.address_mode_skip(mode);
        Instruction::Junk(code, mode)
      }
    }
  }

  pub fn address_mode(&mut self, address_mode: AM) -> Mode {
    match address_mode {
      AM::ABS => Mode::Absolute(self.next_word()),
      AM::ABX => Mode::AbsoluteIndexed(self.next_word(), Index::X),
      AM::ABY => Mode::AbsoluteIndexed(self.next_word(), Index::Y),
      AM::IMM => Mode::Constant(self.next_byte()),
      AM::IMP => unreachable!(),
      AM::IND => unreachable!(),
      AM::INX => Mode::IndirectIndexed(self.next_byte(), Index::X),
      AM::INY => Mode::IndirectIndexed(self.next_byte(), Index::Y),
      AM::REL => unreachable!(),
      AM::ZPG => Mode::ZeroPage(self.next_byte()),
      AM::ZPX => Mode::ZeroPageIndexed(self.next_byte(), Index::X),
      AM::ZPY => Mode::ZeroPageIndexed(self.next_byte(), Index::Y),
    }
  }

  fn address_mode_skip(&mut self, address_mode: AM) {
    match address_mode {
      AM::IMP => return,
      AM::ZPG | AM::ZPX | AM::ZPY | AM::IMM | AM::INX | AM::INY | AM::REL => {
        self.next_byte();
      }
      AM::ABS | AM::ABX | AM::ABY | AM::IND => {
        self.next_word();
      }
    }
  }

  pub fn next_branch_target(&mut self) -> u16 {
    let offset = self.next_byte() as i8;
    self.address.wrapping_add(offset as u16)
  }

  pub fn next_word(&mut self) -> u16 {
    let lower = self.next_byte();
    let upper = self.next_byte();
    u16::from_le_bytes([lower, upper])
  }

  pub fn next_byte(&mut self) -> u8 {
    let val = self.cart.prg_byte(self.address);
    self.address = self.address.wrapping_add(1);
    val
  }
}

#[derive(Debug, Clone, Copy)]
pub enum Instruction {
  ADC(Mode),
  AND(Mode),
  ASL(Mode),
  ASLA,
  BIT(Mode),
  BRK(u8),
  BXX(u16, Condition),
  CLC,
  CLD,
  CLI,
  CLV,
  CMP(Mode),
  CPX(Mode),
  CPY(Mode),
  DEC(Mode),
  DEX,
  DEY,
  EOR(Mode),
  INC(Mode),
  INX,
  INY,
  JMP(u16),
  JSR(u16),
  LDA(Mode),
  LDX(Mode),
  LDY(Mode),
  LSR(Mode),
  LSRA,
  NOP,
  ORA(Mode),
  PHA,
  PHP,
  PLA,
  PLP,
  ROL(Mode),
  ROLA,
  ROR(Mode),
  RORA,
  RTI,
  RTS,
  SBC(Mode),
  SEC,
  SED,
  SEI,
  STA(Mode),
  STX(Mode),
  STY(Mode),
  TAX,
  TAY,
  TSX,
  TXA,
  TXS,
  TYA,
  VIA(u16),
  // Where it belongs
  Junk(OP, AM),
}

#[derive(Debug, Clone, Copy)]
pub enum Mode {
  Absolute(u16),
  AbsoluteIndexed(u16, Index),
  Constant(u8),
  IndirectIndexed(u8, Index),
  ZeroPage(u8),
  ZeroPageIndexed(u8, Index),
}

#[derive(Debug, Clone, Copy)]
pub enum Index {
  X,
  Y,
}

#[derive(Debug, Clone, Copy)]
pub enum Condition {
  CC,
  CS,
  EQ,
  MI,
  NE,
  PL,
  VC,
  VS,
}

impl Condition {
  pub fn dual(self) -> Self {
    match self {
      Condition::CC => Condition::CS,
      Condition::CS => Condition::CC,
      Condition::EQ => Condition::NE,
      Condition::MI => Condition::PL,
      Condition::NE => Condition::EQ,
      Condition::PL => Condition::MI,
      Condition::VC => Condition::VS,
      Condition::VS => Condition::VC,
    }
  }
}

macro_rules! table {
  [$($op:ident $am:ident,)+] => {
    [$((OP::$op, AM::$am),)+]
  }
}

pub const TABLE: [(OP, AM); 256] = table![
  BRK IMM, ORA INX, NIL IMP, NIL INX, NOP ZPG, ORA ZPG, ASL ZPG, NIL ZPG, PHP IMP, ORA IMM, ASL IMP, NIL IMM, NOP ABS, ORA ABS, ASL ABS, NIL ABS,
  BPL REL, ORA INY, NIL IMP, NIL INY, NOP ZPX, ORA ZPX, ASL ZPX, NIL ZPX, CLC IMP, ORA ABY, NOP IMP, NIL ABY, NOP ABX, ORA ABX, ASL ABX, NIL ABX,
  JSR ABS, AND INX, NIL IMP, NIL INX, BIT ZPG, AND ZPG, ROL ZPG, NIL ZPG, PLP IMP, AND IMM, ROL IMP, NIL IMM, BIT ABS, AND ABS, ROL ABS, NIL ABS,
  BMI REL, AND INY, NIL IMP, NIL INY, NOP ZPX, AND ZPX, ROL ZPX, NIL ZPX, SEC IMP, AND ABY, NOP IMP, NIL ABY, NOP ABX, AND ABX, ROL ABX, NIL ABX,
  RTI IMP, EOR INX, NIL IMP, NIL INX, NOP ZPG, EOR ZPG, LSR ZPG, NIL ZPG, PHA IMP, EOR IMM, LSR IMP, NIL IMM, JMP ABS, EOR ABS, LSR ABS, NIL ABS,
  BVC REL, EOR INY, NIL IMP, NIL INY, NOP ZPX, EOR ZPX, LSR ZPX, NIL ZPX, CLI IMP, EOR ABY, NOP IMP, NIL ABY, NOP ABX, EOR ABX, LSR ABX, NIL ABX,
  RTS IMP, ADC INX, NIL IMP, NIL INX, NOP ZPG, ADC ZPG, ROR ZPG, NIL ZPG, PLA IMP, ADC IMM, ROR IMP, NIL IMM, JMP IND, ADC ABS, ROR ABS, NIL ABS,
  BVS REL, ADC INY, NIL IMP, NIL INY, NOP ZPX, ADC ZPX, ROR ZPX, NIL ZPX, SEI IMP, ADC ABY, NOP IMP, NIL ABY, NOP ABX, ADC ABX, ROR ABX, NIL ABX,
  NOP IMM, STA INX, NIL IMM, NIL INX, STY ZPG, STA ZPG, STX ZPG, NIL ZPG, DEY IMP, NOP IMM, TXA IMP, NIL IMM, STY ABS, STA ABS, STX ABS, NIL ABS,
  BCC REL, STA INY, NIL IMP, NIL INY, STY ZPX, STA ZPX, STX ZPY, NIL ZPY, TYA IMP, STA ABY, TXS IMP, NIL ABY, NIL ABX, STA ABX, NIL ABY, NIL ABY,
  LDY IMM, LDA INX, LDX IMM, NIL INX, LDY ZPG, LDA ZPG, LDX ZPG, NIL ZPG, TAY IMP, LDA IMM, TAX IMP, NIL IMM, LDY ABS, LDA ABS, LDX ABS, NIL ABS,
  BCS REL, LDA INY, NIL IMP, NIL INY, LDY ZPX, LDA ZPX, LDX ZPY, NIL ZPY, CLV IMP, LDA ABY, TSX IMP, NIL ABY, LDY ABX, LDA ABX, LDX ABY, NIL ABY,
  CPY IMM, CMP INX, NOP IMM, NIL INX, CPY ZPG, CMP ZPG, DEC ZPG, NIL ZPG, INY IMP, CMP IMM, DEX IMP, NIL IMM, CPY ABS, CMP ABS, DEC ABS, NIL ABS,
  BNE REL, CMP INY, NIL IMP, NIL INY, NOP ZPX, CMP ZPX, DEC ZPX, NIL ZPX, CLD IMP, CMP ABY, NOP IMP, NIL ABY, NOP ABX, CMP ABX, DEC ABX, NIL ABX,
  CPX IMM, SBC INX, NOP IMM, NIL INX, CPX ZPG, SBC ZPG, INC ZPG, NIL ZPG, INX IMP, SBC IMM, NOP IMP, NIL IMM, CPX ABS, SBC ABS, INC ABS, NIL ABS,
  BEQ REL, SBC INY, NIL IMP, NIL INY, NOP ZPX, SBC ZPX, INC ZPX, NIL ZPX, SED IMP, SBC ABY, NOP IMP, NIL ABY, NOP ABX, SBC ABX, INC ABX, NIL ABX,
];

#[derive(Debug, Clone, Copy)]
pub enum OP {
  ADC,
  AND,
  ASL,
  BCC,
  BCS,
  BIT,
  BPL,
  BMI,
  BRK,
  BVC,
  BVS,
  BNE,
  BEQ,
  CLC,
  CLD,
  CLI,
  CLV,
  CMP,
  CPX,
  CPY,
  DEC,
  DEX,
  DEY,
  EOR,
  INC,
  INX,
  INY,
  JMP,
  JSR,
  LDA,
  LDX,
  LDY,
  LSR,
  NOP,
  ORA,
  PHA,
  PHP,
  PLA,
  PLP,
  ROL,
  ROR,
  RTI,
  RTS,
  SBC,
  SEC,
  SED,
  SEI,
  STA,
  STX,
  STY,
  TAX,
  TAY,
  TSX,
  TXA,
  TXS,
  TYA,
  // unintended instruction. only for completion of the table, they will never be supported.
  NIL,
}

#[derive(Debug, Clone, Copy)]
pub enum AM {
  ABS,
  ABX,
  ABY,
  IMM,
  IMP,
  IND,
  INX,
  INY,
  REL,
  ZPG,
  ZPX,
  ZPY,
}
