use std::collections::HashSet;

use petgraph::{dot::Dot, graphmap::DiGraphMap, visit::Dfs, Direction};

type Graph = DiGraphMap<u16, Flow>;
type Search = Dfs<u16, HashSet<u16>>;

use crate::{
  cart::Cartridge,
  cpu::{Condition, Instruction, ProgramCursor},
};

pub struct ControlFlowGraph {
  graph: Graph,
}

impl ControlFlowGraph {
  pub fn from_cartridge(cart: &Cartridge) -> Self {
    let mut tracer = ControlFlowTracer::new(cart);
    tracer.vector(Vector::NMI);
    tracer.vector(Vector::RES);
    tracer.vector(Vector::IRQ);
    tracer.into_graph()
  }
}

impl std::fmt::Debug for ControlFlowGraph {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{:?}", Dot::new(&self.graph))
  }
}

struct ControlFlowTracer<'a> {
  cart: &'a Cartridge,
  graph: Graph,
  seen: HashSet<(Caller, u16)>,
}

impl<'a> ControlFlowTracer<'a> {
  pub fn new(cart: &'a Cartridge) -> Self {
    Self {
      cart,
      graph: Default::default(),
      seen: Default::default(),
    }
  }

  fn vector(&mut self, vector: Vector) {
    let address = vector.into_address();
    let sub = self.cart.prg_word(address);
    let caller = Caller::Vector(vector);
    self.follow(caller, sub)
  }

  fn follow(&mut self, caller: Caller, sub: u16) {
    let mut cursor = ProgramCursor::new(self.cart, sub);

    loop {
      let src = cursor.address();
      let instruction = cursor.next_instruction();
      let ret = cursor.address();

      if !self.seen.insert((caller, src)) {
        return;
      }

      match instruction {
        Instruction::BRK(_) => {
          let dst = self.cart.irq_vector();
          self.connect(src, dst, Flow::BRK);
          self.follow(Caller::BRK(ret), dst);
          continue;
        }
        Instruction::BXX(dst, condition) => {
          self.connect(src, dst, condition);
          self.connect(src, ret, condition.dual());
          self.follow(caller, dst);
          continue;
        }
        Instruction::JMP(dst) => {
          self.connect(src, dst, Flow::JMP);
          self.follow(caller, dst);
          return;
        }
        Instruction::JSR(dst) => {
          self.connect(src, dst, Flow::JSR);
          self.follow(Caller::JSR(ret), dst);
          continue;
        }
        Instruction::RTI => {
          self.rti_or_rts(caller, src, Flow::RTI);
          return;
        }
        Instruction::RTS => {
          self.rti_or_rts(caller, src, Flow::RTS);
          return;
        }
        Instruction::VIA(ptr) => {
          self.connect(src, ptr, Flow::VIA);
          return;
        }
        _ => {
          self.connect(src, ret, Flow::Sequential);
          continue;
        }
      }
    }
  }

  fn rti_or_rts(&mut self, caller: Caller, src: u16, flow: Flow) {
    match caller {
      Caller::BRK(ret) => self.connect(src, ret, flow),
      Caller::JSR(ret) => self.connect(src, ret, flow),
      Caller::Vector(_) => self.connect(src, src, flow),
    }
  }

  fn connect(&mut self, src: u16, dst: u16, flow: impl Into<Flow>) {
    self.graph.add_edge(src, dst, flow.into());
  }

  fn into_graph(mut self) -> ControlFlowGraph {
    self.collapse_nodes(self.cart.nmi_vector());
    self.collapse_nodes(self.cart.res_vector());
    self.collapse_nodes(self.cart.irq_vector());

    ControlFlowGraph { graph: self.graph }
  }

  fn collapse_nodes(&mut self, address: u16) {
    while let Some((node, prev, next)) = self.next_collapsible_node(address) {
      self.graph.remove_node(node);
      self.graph.add_edge(prev, next, Flow::Sequential);
    }
  }

  fn next_collapsible_node(&self, address: u16) -> Option<(u16, u16, u16)> {
    let mut search = Search::new(&self.graph, address);

    while let Some(node) = search.next(&self.graph) {
      let incoming = self.graph.edges_directed(node, Direction::Incoming).collect::<Vec<_>>();
      let outgoing = self.graph.edges_directed(node, Direction::Outgoing).collect::<Vec<_>>();

      if incoming.len() == 1 && outgoing.len() == 1 {
        let (prev, _, Flow::Sequential) = incoming[0] else { continue };
        let (_, next, Flow::Sequential) = outgoing[0] else { continue };

        return Some((node, prev, next));
      }
    }

    None
  }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
enum Caller {
  BRK(u16),
  JSR(u16),
  Vector(Vector),
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
enum Vector {
  IRQ,
  NMI,
  RES,
}

impl Vector {
  pub fn into_address(self) -> u16 {
    match self {
      Self::IRQ => 0xfffa,
      Self::NMI => 0xfffc,
      Self::RES => 0xfffe,
    }
  }
}

#[derive(Clone, Copy, Debug)]
pub enum Flow {
  Sequential,
  B(Condition),
  BRK,
  JMP,
  JSR,
  RTI,
  RTS,
  VIA,
}

impl From<Condition> for Flow {
  fn from(condition: Condition) -> Self {
    Self::B(condition)
  }
}
