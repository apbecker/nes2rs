pub trait Pairwise
where
  Self: Iterator,
{
  fn pairwise(self) -> PairwiseIter<Self>;
}

impl<T> Pairwise for T
where
  Self: Iterator,
{
  fn pairwise(mut self) -> PairwiseIter<Self> {
    PairwiseIter {
      item: self.next(),
      iter: self,
    }
  }
}

pub struct PairwiseIter<T>
where
  T: Iterator + ?Sized,
{
  item: Option<T::Item>,
  iter: T,
}

impl<T> Iterator for PairwiseIter<T>
where
  T: Iterator,
  T::Item: Clone + Copy,
{
  type Item = (T::Item, T::Item);

  fn next(&mut self) -> Option<Self::Item> {
    let a = self.item?;
    self.item = self.iter.next();
    let b = self.item?;

    Some((a, b))
  }
}

#[test]
pub fn pairwise_works() {
  let mut items = vec![0, 3, 6, 9].into_iter().pairwise();

  assert_eq!(items.next(), Some((0, 3)));
  assert_eq!(items.next(), Some((3, 6)));
  assert_eq!(items.next(), Some((6, 9)));
  assert_eq!(items.next(), None);
}
