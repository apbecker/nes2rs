use std::io;

pub trait ReadExactVec {
  /// Allocates a new `Vec<u8>` and fills it with exactly `len` bytes.
  fn read_exact_vec(&mut self, len: usize) -> io::Result<Vec<u8>>;
}

impl<R: io::Read> ReadExactVec for R {
  fn read_exact_vec(&mut self, len: usize) -> io::Result<Vec<u8>> {
    let mut buf = vec![0; len];
    self.read_exact(&mut buf)?;
    Ok(buf)
  }
}
